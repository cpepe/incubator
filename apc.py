import sys
import getopt
import serial
from time import sleep
import logging
logging.basicConfig(filename="serial.log", level=logging.INFO)
log = logging.getLogger('serial')

DEV='/dev/ttyUSB0'
dev=DEV

class PortController():
    def __init__(self):
        pass
    def on(self):
        s = serial.Serial(dev, 9600, timeout=2, rtscts=0, xonxoff=0, parity=serial.PARITY_NONE)
        if s.isOpen():
            log.info( 'Serial port %s is open' % (s.portstr))
        else:
            log.info( 'Serial port %s cannot be opened' % (s.portstr))
        log.info( 'Flushing buffer')
        log.info( 'Turning port 7 on')
        s.write('n003E')
        sleep(1)
        s.close()
    def off(self):
        s = serial.Serial(dev, 9600, timeout=2, rtscts=0, xonxoff=0, parity=serial.PARITY_NONE)
        if s.isOpen():
            log.info( 'Serial port %s is open' % (s.portstr))
        else:
            log.info( 'Serial port %s cannot be opened' % (s.portstr))
        log.info( 'Flushing buffer')
        log.info( 'Turning port 7 on')
        s.write('n000E')
        sleep(1)
        s.close()

