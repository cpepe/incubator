 #!/usr/bin/env python
"""
Simple data logger to watch compost temps and save to CSV format

Format: event, sensor #, timestamp, ambient, sensor temp
events: temperature, fan_on, fan_off
timestamp: ts=time.time(), display with time.ctime( ts )

"""


"""Copyright 2010 Phidgets Inc.
This work is licensed under the Creative Commons Attribution 2.5 Canada License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/2.5/ca/
"""

__author__ = 'Christopher Pepe'
__version__ = '0.1'
__date__ = '2014'

from apc import PortController

#Cmdline option handling
import sys
import getopt
#already done below: from time import time

#Basic imports
from ctypes import *
import sys
import logging
from time import sleep, time, ctime
#Phidget specific imports
from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, TemperatureChangeEventArgs
from Phidgets.Devices.TemperatureSensor import TemperatureSensor, ThermocoupleType

#Setup logging to use a meaningful logfile name
#set default batch name
dfl_batchname = 'natto-%s' % ( int(time()) )
try:
    #allow cmdline overrides
    opts,args = getopt.gnu_getopt(sys.argv[1:], 'b:dummy')
    opts = dict(opts)
    batch = opts.get('-b', dfl_batchname)
except Exception, e:
    batch = dfl_batchname

#new power controller guy
power = PortController()

#temperatures in C
holdTemp = 39.5 #34.4=96F, #37.8=100F #40=104
lastHighTempTime = time() #start with now and grow the error slowly
maxHeaterOnTime = 80

print 'Logging to logs/%s.log' % (batch)
logging.basicConfig(filename='logs/%s.log' % (batch), level=logging.INFO, format='', mode='a')
log = logging.getLogger(batch)
#Create an temperaturesensor object
try:
    temperatureSensor = TemperatureSensor()
except RuntimeError as e:
    print("Runtime Exception: %s" % e.details)
    print("Exiting....")
    exit(1)

def readT():
    avgC = 0
    for i in range(0,5):
        tC = temperatureSensor.getTemperature(0)
        tF = tC*9/5.0+32
        avgC += tC
        print "Now reading %0.2fC %0.2fF" % (tC, tF)
        sleep(0.25)
    avgC = avgC/5.0
    datapoint = "%s,%i,%f,%0.3f,%0.3f" % (
            'temperature',
            0,
            time(),
            temperatureSensor.getAmbientTemperature(),
            avgC)
    log.info(datapoint)
    print datapoint

def fan():
    datapoint = "%s,%i,%f,%s,%0.3f" % (
        'fan',
        0,
        time(),
        '',
        30)
    log.info(datapoint)
    print datapoint

def heatOff():
    print("Turning heater off")
    power.off()

def heatOn():
    print("Turning heater on")
    power.on()

def handleStdin():
    cmds = ('r','f','c','h')
    char = sys.stdin.readline()
    while char[0] in cmds:
        if char[0] == 'r':
            readT()
        elif char[0] == 'f':
            readT()
            fan()
        elif char[0] == 'c':
            heatOff()
        elif char[0] == 'h':
            heatOn()
        print("Press Enter to quit (r - read, f - fan, c - cool, h - heat)")
        avgC = 0
        char = sys.stdin.readline()

#Information Display Function
def DisplayDeviceInfo():
    inputCount = temperatureSensor.getTemperatureInputCount()
    print("|------------|----------------------------------|--------------|------------|")
    print("|- Attached -|-              Type              -|- Serial No. -|-  Version -|")
    print("|------------|----------------------------------|--------------|------------|")
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (temperatureSensor.isAttached(), temperatureSensor.getDeviceName(), temperatureSensor.getSerialNum(), temperatureSensor.getDeviceVersion()))
    print("|------------|----------------------------------|--------------|------------|")
    print("Number of Temperature Inputs: %i" % (inputCount))
    for i in range(inputCount):
        print("Input %i Sensitivity: %f" % (i, temperatureSensor.getTemperatureChangeTrigger(i)))

#Event Handler Callback Functions
def TemperatureSensorAttached(e):
    attached = e.device
    print("TemperatureSensor %i Attached!" % (attached.getSerialNum()))

def TemperatureSensorDetached(e):
    detached = e.device
    print("TemperatureSensor %i Detached!" % (detached.getSerialNum()))

def TemperatureSensorError(e):
    source = e.device
    print("TemperatureSensor %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))

def TemperatureSensorTemperatureChanged(e):
    global lastHighTempTime
    try:
        ambient = temperatureSensor.getAmbientTemperature()
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))
        ambient = 0.00
    
    source = e.device
    print ("TemperatureSensor %i: Ambient Temp: %f -- Thermocouple %i temperature: %0.2fC %0.2fF -- Potential: %f" % (source.getSerialNum(), ambient, e.index, e.temperature, 9*e.temperature/5.0+32, e.potential))
    #event, sensor #, timestamp, ambient, sensor temp
    datapoint = "%s,%i,%f,%0.3f,%0.3f" % (
        'temperature', 
        e.index,
        time(),
        ambient, 
        e.temperature)
    log.info(datapoint)
    
    #handle temperature control here
    if ambient != 0.00:
        if e.temperature > holdTemp:
            print('Updated last hot temp timestamp')
            lastHighTempTime = time()
        
        if e.temperature > holdTemp:
            #too hot, shut off heater
            print("Too Hot, shutting down")
            power.off()
        elif e.temperature < holdTemp:
            #too cold, turn on heater       
            power.on()
            # error = 10s per deg off and 10s per minute cold
            now = time()
            temporalError = (now - lastHighTempTime)/60.0 #diff in minutes
            temperatureError = (holdTemp - e.temperature) #diff in deg C
            error = temporalError + temperatureError
            print('Too cold, heater on! (last time I was warm was %s min ago)' % (temporalError))
            if temperatureError < 5 or error > 8:
                print("Close to goal, heating for %s sec" % (min(10*error, maxHeaterOnTime)))
                #if within 5 deg of hold temp
                #heat for 20s per degree off, then wait 30s for change to take place before leaving
                sleep( min(20*error, maxHeaterOnTime) )
                power.off()
                print("Done heating cycle resting for 30s") #should be proportional to error as well and non-blocking
                sleep(30)
                print("Done resting")



#Main Program Code
log.info('#Started monitoring at %s' % (ctime( time() )))
try:
    temperatureSensor.setOnAttachHandler(TemperatureSensorAttached)
    temperatureSensor.setOnDetachHandler(TemperatureSensorDetached)
    temperatureSensor.setOnErrorhandler(TemperatureSensorError)
    temperatureSensor.setOnTemperatureChangeHandler(TemperatureSensorTemperatureChanged)
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Opening phidget object....")

try:
    temperatureSensor.openPhidget()
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Waiting for attach....")

try:
    temperatureSensor.waitForAttach(10000)
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    try:
        temperatureSensor.closePhidget()
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))
        print("Exiting....")
        exit(1)
    print("Exiting....")
    exit(1)
else:
    DisplayDeviceInfo()

print("Setting sensitivity of the thermocouple to 0.15....")
temperatureSensor.setTemperatureChangeTrigger(0, 0.15)

print("Press Enter to quit (r to read, f for fan)....")
handleStdin()
print("Closing...")
log.info('#Stopped monitoring at %s' % (ctime( time() )))
try:
    temperatureSensor.closePhidget()
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Done.")
exit(0)
