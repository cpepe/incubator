import time

convertDate = False
labels = ('Event', 'Sensor ID', 'Date', 'Ambient', 'Compost')

def convertLogToCsv():
    dps = list()
    f=open('event.log')
    o = open('event.csv', 'w')
    r=f.read();f.close()
    r=r.split('\n')
    for i in r:
        if not i.startswith('#') and len(i)>0:
            #print 'Working %s' % i
            ps = i.split(',')
            if convertDate is True:
                dt = time.ctime(float(ps[2]))
                ps[2] = dt
            o.write(','.join(ps))
            o.write('\n')
            dps.append(ps)
    o.close()
    return dps

def transpose(data):
    """
    turn 1,2,3,4
         5,6,7,8
    into
         1,5
         2,6
         3,7
         4,8
    """
    out = dict()
    #for d in data:
    lenD = len(data)
    for i in range(lenD-150, lenD):
        d=data[i]
        for index in range(0, len(d)):
            if index in out:
                out[index].append( d[index] )
            else:
                out[index] = [ d[index], ]
    for key, row in out.iteritems():
        title = labels[key]
        line = ','.join(row)
        print '%s,%s' % (title, line)
    

if __name__ == "__main__":
    mydps = convertLogToCsv()
    transpose(mydps)

