# README #

This project assumes that you have a phidget thermocouple device, an electric heater (I use a rice cooker), and a cooler that you can plumb this junk into to make a simple incubator.

This type of system has a ton of dampening and is slow to respond so that is accounted for in the control loop. Heat is applied for a short period of time and a pause is allowed for the system to respond before deciding whether to continue heating or not.

Initially there is a lot of blocking and other poor embedded control techniques used. I'll probably fix that in my gobs of spare time.

### How do I get set up? ###

* Install libphidget
* Run as root (sadly yes, needed for the phidget - unless you find a way around that)

### Configuration ###
#### holdTemp ####
Defaults to 37.8C (100F). This is the temperature that the incubator will try to hold at

#### variance ####
Defaults to 0.1. This is how much wobble in temperature is allowed (in practice you get waaaaay more but this is here for now)

#### lastHighTempTime ####
The last time the incubator was at the holdTemp. Initialized to now() so that the temporal error starts off small and grows as the incubator heats up
lastHighTempTime = time() #start with now and grow the error slowly

#### maxHeaterOnTime ####
Defaults to 60s. This is the maximum amount of time a heating cycle can run. Based on the temperature and temporal error the heater will run for some number of seconds. This keeps the upper limit bound to a reasonable value.

### Contribution guidelines ###

* Do if you want to
* Otherwise don't

### Who do I talk to? ###

* Repo owner or admin
* Yourself